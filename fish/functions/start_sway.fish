function start_sway
    set -x XDG_SESSION_TYPE wayland
    set -x XDG_CURRENT_DESKTOP sway
    set -x XDG_SESSION_DESKTOP sway
    set -x QT_QPA_PLATFORM wayland
    set -x QT_QPA_PLATFORMTHEME xcb
    set -x GTK_THEME Nordic-darker
    set -x SDL_VIDEODRIVER wayland
    set -x _JAVA_AWT_WM_NONREPARENTING 1
    set -x NO_AT_BRIDGE 1
    set -x BEMENU_BACKEND wayland
    set -x MOZ_ENABLE_WAYLAND 1
    set -x MOZ_DBUS_REMOTE 1
    set -x SSH_AUTH_SOCK "$XDG_RUNTIME_DIR/ssh-agent"
    set -x EDITOR micro
    set -x BROWSER librewolf
    sway
end
