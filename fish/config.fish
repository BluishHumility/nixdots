## Set values
# Hide welcome message
set fish_greeting
set VIRTUAL_ENV_DISABLE_PROMPT "1"

# Man pages
set -x MANROFFOPT "-c"
set -x MANPAGER "sh -c 'col -bx | bat -l man -p'"

# Start Sway when logging in from TTY1
# if [ /dev/pts/1 = /dev/tty1 ]
#     sway-run
# end

if test (tty) = /dev/tty1
    start_sway
end

# Starship
starship init fish | source

## Useful aliases
# Replace ls with exa
alias ls='exa -alg --color=always --group-directories-first' # preferred listing
alias la='exa -a --color=always --group-directories-first --icons'  # all files and dirs
alias ll='exa -l --color=always --group-directories-first --icons'  # long format
alias lt='exa -aT --color=always --group-directories-first --icons' # tree listing
alias ip="ip -color"

# Replace some more things with better alternatives
alias cat='bat --style header --style snip --style changes --style header'

# Common use
alias tarnow='tar -acf '
alias untar='tar -xvf '
alias wget='wget -c '
alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
alias .....='cd ../../../..'
alias ......='cd ../../../../..'
alias dir='dir --color=auto'
alias vdir='vdir --color=auto'
alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'
alias hw='hwinfo --short'
alias tb='nc termbin.com 9999'

# Get the error messages from journalctl
alias jctl="journalctl -p 3 -xb"

function set_vconsole_colors
    echo -en "\e]P00D0F13"
    echo -en "\e]P1BF616A"
    echo -en "\e]P2A3BE8C"
    echo -en "\e]P3EBCB8B"
    echo -en "\e]P481A1C1"
    echo -en "\e]P5B48EAD"
    echo -en "\e]P688C0D0"
    echo -en "\e]P7E5E9F0"
    echo -en "\e]P84C566A"
    echo -en "\e]P9BF616A"
    echo -en "\e]PAA3BE8C"
    echo -en "\e]PBEBCB8B"
    echo -en "\e]PCB48EAD"
    echo -en "\e]PD8FBCBB"
    echo -en "\e]PEECEFF4"
end

# Call the function to set colors
set_vconsole_colors
